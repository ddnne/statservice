﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Statistics.Probability;
using Statistics.Probability.Continuous;

namespace Chart.MainWindow
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void chart1_Click(object sender, EventArgs e)
        {
            //AddSin();
            //AddCos();
            AddDist();
        }

        public void Plot(ChartData data)
        {
            if(!chart1.Series.Where(s => s.Name == data.Title).Any())
                chart1.Series.Add(data.Title);

            data.Xs.Zip(data.Ys, (x, y) => (x, y)).Select(xy => chart1.Series[data.Title].Points.AddXY(xy.x, xy.y)).ToList();
            chart1.Series[data.Title].ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), data.SeriesChartType);
        }

        private void AddSin()
        {
            var xs = Enumerable.Range(0, 100).Select(x => x * 0.1);
            var sins = xs.Select(x => Math.Sin(x));

            Plot(new ChartData("sin(x)", xs, sins));
            chart1.Refresh();
        }

        private void AddCos()
        {
            var xs = Enumerable.Range(0, 100).Select(x => x * 0.1);
            var coss = xs.Select(x => Math.Cos(x));

            Plot(new ChartData("cos(x)", xs, coss));
            chart1.Refresh();
        }

        private void AddDist()
        {
            int size = 100;
            using (var param = new UniformParameter(1.5, 2))
            {
                using (var dist = new Uniform(param))
                {
                    var xs = dist.GetRandoms(size);
                    var ys = xs.Select(x => dist.GetProbabilityDensity(x));
                    Plot(new ChartData("Dist", xs, ys, "Point"));
                }
            }
            chart1.Refresh();
        }
    }
}
