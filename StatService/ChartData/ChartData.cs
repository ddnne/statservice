﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Chart
{
    public class ChartData : IEquatable<ChartData>
    {
        /// <param name="seriesChartType"> string ver of System.Windows.Forms.DataVisualization.Charting.SeriesChartType</param>
        public ChartData(string title, IEnumerable<double> xs, IEnumerable<double> ys, string seriesChartType = "Line")
        {
            Title = title;
            Xs = xs;
            Ys = ys;
            SeriesChartType = seriesChartType;
        }

        public ChartData(string title, IEnumerable<(double x, double y)> xys, string seriesChartType = "Line")
        {
            Title = title;
            Xs = xys.Select(xy => xy.x);
            Ys = xys.Select(xy => xy.y);
            SeriesChartType = seriesChartType;
        }

        public string Title { get; }
        public IEnumerable<double> Xs { get; }
        public IEnumerable<double> Ys { get; }
        public string SeriesChartType { get; }

        public override bool Equals(object obj)
        {
            return Equals(obj as ChartData);
        }

        public bool Equals(ChartData other)
        {
            return other != null &&
                   Title == other.Title &&
                   EqualityComparer<IEnumerable<double>>.Default.Equals(Xs, other.Xs) &&
                   EqualityComparer<IEnumerable<double>>.Default.Equals(Ys, other.Ys);
        }

        public override int GetHashCode()
        {
            var hashCode = -1295154182;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Title);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<double>>.Default.GetHashCode(Xs);
            hashCode = hashCode * -1521134295 + EqualityComparer<IEnumerable<double>>.Default.GetHashCode(Ys);
            return hashCode;
        }
    }
}
