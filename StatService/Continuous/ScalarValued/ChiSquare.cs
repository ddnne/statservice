﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics.Probability.Continuous
{
    public class ChiSquare: Gamma
    {
        public ChiSquare(ChiSquareParameter parameter): base(parameter) { }
    }
}
