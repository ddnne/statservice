﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Accord.Statistics.Distributions.Univariate;

namespace Statistics.Probability.Continuous
{
    public class Gamma : AContinuous<double>, IEquatable<Gamma>
    {
        private GammaDistribution Distribution { get; }

        public Gamma(IParameter parameter):base(parameter)
        {
            if (!(parameter is GammaParameter))
                throw new ApplicationException();
            Distribution = new GammaDistribution(((GammaParameter)Parameter).Theta, ((GammaParameter)Parameter).K);
        }

        public override double GetProbabilityDensity(double randomValue)
        {
            return Distribution.ProbabilityDensityFunction(randomValue);
        }

        public override IEnumerable<double> GetRandoms(int size)
        {
            return Distribution.Generate(size);
        }

        public override Func<IParameter, double> GetLikelihoodFunction(double randomValue)
        {
            return (IParameter param) =>
            {
                var gamma = new Gamma(param);
                return gamma.GetProbabilityDensity(randomValue);
            };
        }

        public bool Equals(IProbability<double> other)
        {
            if (!(other is Gamma))
                return false;
            else
                return Equals((Gamma)other);
        }

        public override string ToString(string format, IFormatProvider formatProvider)
        {
            return "Distribution:\t Gamma\n" + Parameter.ToString();
        }

        public bool Equals(Gamma other)
        {
            return other != null &&
                   EqualityComparer<IParameter>.Default.Equals(Parameter, other.Parameter) &&
                   EqualityComparer<GammaDistribution>.Default.Equals(Distribution, other.Distribution);
        }

        public override int GetHashCode()
        {
            var hashCode = -1449207421;
            hashCode = hashCode * -1521134295 + EqualityComparer<IParameter>.Default.GetHashCode(Parameter);
            hashCode = hashCode * -1521134295 + EqualityComparer<GammaDistribution>.Default.GetHashCode(Distribution);
            return hashCode;
        }
    }
}
