﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics.Probability.Continuous
{
    public class ChiSquareParameter : GammaParameter
    {
        public ChiSquareParameter(int n) : base(2, n / 2.0)
        {
        }

        public ChiSquareParameter(ChiSquareParameter parameter) : base(2, parameter.K)
        {
        }

        public bool Equals(ChiSquareParameter other)
        {
            return other != null &&
                other.K == K;
        }

        public override bool Equals(IParameter other)
        {
            if (!(other is ChiSquareParameter))
                return false;
            else
                return Equals((ChiSquareParameter)other);
        }

        public override string ToString()
        {
            return $"n:\t {(int)(K * 2)}";
        }
    }
}
