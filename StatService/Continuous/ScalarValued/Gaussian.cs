﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Statistics.Distributions.Univariate;

namespace Statistics.Probability.Continuous
{
    public class Gaussian : AContinuous<double>, IEquatable<Gaussian>
    {
        public Gaussian(IParameter parameter):base(parameter)
        {
            if (!(parameter is GaussianParameter))
                throw new ApplicationException();

            Distribution = new NormalDistribution(((GaussianParameter)Parameter).Mean, ((GaussianParameter)Parameter).Sigma);
        }

        private NormalDistribution Distribution { get; }
        
        public bool Equals(IProbability<double> other)
        {
            if (!(other is Gaussian))
                return false;
            else
                return other.GetParameter().Equals(this.Parameter);
        }

        public bool Equals(Gaussian other)
        {
            return other != null &&
                   EqualityComparer<IParameter>.Default.Equals(Parameter, other.Parameter) &&
                   EqualityComparer<NormalDistribution>.Default.Equals(Distribution, other.Distribution);
        }

        public override int GetHashCode()
        {
            var hashCode = -1309467774;
            hashCode = hashCode * -1521134295 + EqualityComparer<IParameter>.Default.GetHashCode(Parameter);
            hashCode = hashCode * -1521134295 + EqualityComparer<NormalDistribution>.Default.GetHashCode(Distribution);
            return hashCode;
        }

        public override Func<IParameter, double> GetLikelihoodFunction(double randomValue)
        {
            return (IParameter param) =>
            {
                var gaussian = new Gaussian(param);
                return gaussian.GetProbabilityDensity(randomValue);
            };
        }

        public override double GetProbabilityDensity(double randomValue)
        {
            return Distribution.ProbabilityDensityFunction(randomValue);
        }

        public override IEnumerable<double> GetRandoms(int size)
        {
            return Distribution.Generate(size);
        }

        public override string ToString(string format, IFormatProvider formatProvider)
        {
            return "Distribution:\t Gaussian\n" + Parameter.ToString();
        }
    }
}
