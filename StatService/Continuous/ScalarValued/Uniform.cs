﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Statistics.Distributions.Univariate;

namespace Statistics.Probability.Continuous
{
    public class Uniform : AContinuous<double>, IEquatable<Uniform>
    {
        public Uniform(IParameter parameter): base(parameter)
        {
            if (!(parameter is UniformParameter))
                throw new ApplicationException();
            Distribution = new UniformContinuousDistribution(((UniformParameter)Parameter).Start, ((UniformParameter)Parameter).End);
        }

        private UniformContinuousDistribution Distribution { get; }

        public bool Equals(IProbability<double> other)
        {
            return other.GetParameter().Equals(Parameter);
        }

        public bool Equals(Uniform other)
        {
            return other != null &&
                   EqualityComparer<IParameter>.Default.Equals(Parameter, other.Parameter) &&
                   EqualityComparer<UniformContinuousDistribution>.Default.Equals(Distribution, other.Distribution);
        }

        public override int GetHashCode()
        {
            var hashCode = -1449207421;
            hashCode = hashCode * -1521134295 + EqualityComparer<IParameter>.Default.GetHashCode(Parameter);
            hashCode = hashCode * -1521134295 + EqualityComparer<UniformContinuousDistribution>.Default.GetHashCode(Distribution);
            return hashCode;
        }

        public override Func<IParameter, double> GetLikelihoodFunction(double randomValue)
        {
            return (IParameter param) =>
            {
                var uniform = new Uniform(param);
                return uniform.GetProbabilityDensity(randomValue);
            };
        }

        public override double GetProbabilityDensity(double randomValue)
        {
            return Distribution.ProbabilityDensityFunction(randomValue);
        }

        public override IEnumerable<double> GetRandoms(int size)
        {
            return Distribution.Generate(size);
        }

        public override string ToString(string format, IFormatProvider formatProvider)
        {
            return "Distribution:\t Uniform\n" + Parameter.ToString();
        }
    }
}
