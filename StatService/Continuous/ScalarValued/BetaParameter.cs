﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics.Probability.Continuous
{
    public class BetaParameter : IParameter, IEquatable<BetaParameter>
    {
        public BetaParameter(double alpha, double beta)
        {
            Alpha = alpha;
            Beta = beta;
        }
        public BetaParameter(BetaParameter parameter)
        {
            Alpha = parameter.Alpha;
            Beta = parameter.Beta;
        }

        public double Alpha { get; }
        public double Beta { get; }

        public bool Equals(BetaParameter other)
        {
            return other != null &&
                   Alpha == other.Alpha &&
                   Beta == other.Beta;
        }

        public bool Equals(IParameter other)
        {
            if (!(other is BetaParameter))
                return false;
            else
                return Equals((BetaParameter)other);
        }

        public override int GetHashCode()
        {
            var hashCode = -187322134;
            hashCode = hashCode * -1521134295 + Alpha.GetHashCode();
            hashCode = hashCode * -1521134295 + Beta.GetHashCode();
            return hashCode;
        }

        public void Dispose()
        {
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return $"Alpha:\t {Alpha}\nBeta:\t {Beta}";
        }
    }
}
