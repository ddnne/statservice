﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics.Probability.Continuous
{
    public class ExponentialParameter : GammaParameter
    {
        public ExponentialParameter(double theta) : base(theta, 1)
        {
        }

        public ExponentialParameter(ExponentialParameter parameter) : base(parameter.Theta, 1)
        {
        }

        public bool Equals(ExponentialParameter other)
        {
            return other != null &&
                other.Theta == Theta;
        }

        public override bool Equals(IParameter other)
        {
            if (!(other is ExponentialParameter))
                return false;
            else
                return Equals((ExponentialParameter)other);
        }

        public override string ToString()
        {
            return $"Theta:\t {Theta}";
        }
    }
}
