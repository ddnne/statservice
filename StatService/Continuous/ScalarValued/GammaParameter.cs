﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics.Probability.Continuous
{
    public class GammaParameter : IParameter, IEquatable<GammaParameter>
    {
        public double Theta { get; }
        public double K { get; }

        public GammaParameter(double theta, double k)
        {
            Theta = theta;
            K = k;
        }
        public GammaParameter(GammaParameter parameter)
        {
            Theta = parameter.Theta;
            K = parameter.K;
        }

        public void Dispose()
        {
        }

        public virtual bool Equals(IParameter other)
        {
            if (!(other is GammaParameter))
                return false;
            else
                return Equals((GammaParameter)other);
        }

        public virtual string ToString(string format, IFormatProvider formatProvider)
        {
            return $"Theta:\t {Theta}\nK:\t {K}";
        }

        public bool Equals(GammaParameter other)
        {
            return other != null &&
                   Theta == other.Theta &&
                   K == other.K;
        }

        public override int GetHashCode()
        {
            var hashCode = 162027773;
            hashCode = hashCode * -1521134295 + Theta.GetHashCode();
            hashCode = hashCode * -1521134295 + K.GetHashCode();
            return hashCode;
        }
    }
}
