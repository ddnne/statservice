﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Statistics.Probability.Continuous
{
    public class UniformParameter : IParameter, IEquatable<UniformParameter>
    {
        public UniformParameter(double start, double end)
        {
            Start = start;
            End = end;
        }
        public UniformParameter(UniformParameter other)
        {
            Start = other.Start;
            End = other.End;
        }

        public double Start { get; }
        public double End { get; }

        public void Dispose()
        {
        }

        public bool Equals(IParameter other)
        {
            if (!(other is UniformParameter))
                return false;
            else
                return Equals((UniformParameter)other);
        }

        string IFormattable.ToString(string format, IFormatProvider formatProvider)
        {
            return $"Start:\t {Start}\nEnd:\t {End}";
        }

        public bool Equals(UniformParameter other)
        {
            return other != null &&
                   Start == other.Start &&
                   End == other.End;
        }

        public override int GetHashCode()
        {
            var hashCode = -1912830823;
            hashCode = hashCode * -1521134295 + Start.GetHashCode();
            hashCode = hashCode * -1521134295 + End.GetHashCode();
            return hashCode;
        }
    }
}
