﻿using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Statistics.Distributions.Univariate;

namespace Statistics.Probability.Continuous
{
    public class Beta : AContinuous<double>, IEquatable<Beta>
    {
        public Beta(IParameter parameter):base(parameter)
        {
            if (!(parameter is BetaParameter))
                throw new ApplicationException();
            Distribution = new BetaDistribution(((BetaParameter)Parameter).Alpha, ((BetaParameter)Parameter).Beta);
        }

        private BetaDistribution Distribution { get; }
        
        public bool Equals(IProbability<double> other)
        {
            if (!(other is Beta))
                return false;
            else
                return other.GetParameter().Equals(this.Parameter);
        }

        public bool Equals(Beta other)
        {
            return other != null &&
                   EqualityComparer<IParameter>.Default.Equals(Parameter, other.Parameter) &&
                   EqualityComparer<BetaDistribution>.Default.Equals(Distribution, other.Distribution);
        }

        public override int GetHashCode()
        {
            var hashCode = -1309467774;
            hashCode = hashCode * -1521134295 + EqualityComparer<IParameter>.Default.GetHashCode(Parameter);
            hashCode = hashCode * -1521134295 + EqualityComparer<BetaDistribution>.Default.GetHashCode(Distribution);
            return hashCode;
        }

        public override Func<IParameter, double> GetLikelihoodFunction(double randomValue)
        {
            return (IParameter param) =>
            {
                var beta = new Beta(param);
                return beta.GetProbabilityDensity(randomValue);
            };
        }

        public override double GetProbabilityDensity(double randomValue)
        {
            return Distribution.ProbabilityDensityFunction(randomValue);
        }

        public override IEnumerable<double> GetRandoms(int size)
        {
            return Distribution.Generate(size);
        }

        public override string ToString(string format, IFormatProvider formatProvider)
        {
            return "Distribution:\t Beta\n" + Parameter.ToString();
        }
    }
}
