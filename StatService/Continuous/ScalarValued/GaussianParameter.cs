﻿using System;

namespace Statistics.Probability.Continuous
{
    public class GaussianParameter: IParameter
    {
        public GaussianParameter(double mean, double sigma)
        {
            Mean = mean;
            Sigma = sigma;
        }

        public GaussianParameter(GaussianParameter other)
        {
            Mean = other.Mean;
            Sigma = other.Sigma;
        }

        public double Mean { get; }
        public double Sigma { get; }

        public IParameter Clone()
        {
            return new GaussianParameter(this.Mean, this.Sigma);
        }

        public bool Equals(GaussianParameter other)
        {
            return other != null &&
                   Mean == other.Mean &&
                   Sigma == other.Sigma;
        }

        public override int GetHashCode()
        {
            var hashCode = 622076328;
            hashCode = hashCode * -1521134295 + Mean.GetHashCode();
            hashCode = hashCode * -1521134295 + Sigma.GetHashCode();
            return hashCode;
        }

        public bool Equals(IParameter other)
        {
            if (!(other is GaussianParameter))
                return false;
            else
                return Equals((GaussianParameter)other);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            return $"Mean:\t {Mean.ToString()}\nSigma:\t {Sigma.ToString()}";
        }

        public void Dispose()
        {
        }
    }
}
