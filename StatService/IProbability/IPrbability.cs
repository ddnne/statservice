﻿using System;
using System.Collections.Generic;

namespace Statistics.Probability
{
    /// <typeparam name="RandomValue">
    /// type of random value. e.g. double with 1d Gaussian, Vector<double> with multivariate gaussian
    /// </typeparam>
    public interface IProbability<RandomValue> : IFormattable, IDisposable
    {
        RandomValue GetRandom();
        IEnumerable<RandomValue> GetRandoms(int size);
        Func<IParameter,double> GetLikelihoodFunction(RandomValue randomValue);
        IParameter GetParameter();
    }
}