﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Statistics.Probability.Continuous
{
    public abstract class AContinuous<RandomValue> : IProbability<RandomValue>
    {
        public AContinuous(IParameter parameter)
        {
            Parameter = parameter;
        }

        protected IParameter Parameter { get; }


        void IDisposable.Dispose()
        {
            Parameter.Dispose();
        }

        public IParameter GetParameter()
        {
            return Parameter;
        }

        public RandomValue GetRandom()
        {
            return GetRandoms(1).First();
        }

        public abstract Func<IParameter, double> GetLikelihoodFunction(RandomValue randomValue);

        public abstract double GetProbabilityDensity(RandomValue randomValue);

        public abstract IEnumerable<RandomValue> GetRandoms(int size);

        public abstract string ToString(string format, IFormatProvider formatProvider);
    }
}
