﻿using System;

namespace Statistics.Probability
{
    public interface IParameter : IEquatable<IParameter>, IFormattable, IDisposable
    {
    }
}